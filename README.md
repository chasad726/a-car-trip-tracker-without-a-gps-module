# MrDIY Car Trip Tracker

In this project, I show how I track my car's trips using an ESP8266 board ( a Wemos D1 mini) and serve them on a HTTP server using Google's Geolocartion and Maps services.

<br>
**Watch the video**: click on the image below:

[![MrDIY Audio Notifier youtube video](https://img.youtube.com/vi/nHaEYJzduZM/0.jpg)](https://www.youtube.com/watch?v=nHaEYJzduZM)


## Instructions

The <strong>esp8266_geolocation.ino</strong> is a sample code file that demonstrates how Geolocation is used to find the GPS coordinates by sending it the list of surrounding Access Points' MACs.

To test my tracker, simply flash the <strong>.bin</strong> file to a Wemos D1 mini, then open the serial port, type 'setup' and follow the instructions. This is also demonstrated towards the end of the video above.

## Get the Shield

Amazon.com - https://amzn.to/3tZTk7C<br>
AliExpress - https://s.click.aliexpress.com/e/_AZer1e<br>
Amazon.ca: - https://amzn.to/3rpSe3i<br>

(When available, I use affiliate links and may earn a commission)

## Thanks
Many thanks to all the authors and contributors to the open source libraries I have used. 
